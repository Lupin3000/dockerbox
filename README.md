Usage Info
==========

Example Usage (build own image)
-------------------------------

```shell
# create new VM
$ docker-machine create -d virtualbox Streamer

# list VM`s (optional)
$ docker-machine ls

# pointing shell
$ eval $(docker-machine env Streamer)

# build from Dockerfile
$ docker build -t slorenz/streamer .

# list images (optional)
$ docker images

# create new container
$ docker run -d --name streamer -p 80:8080 -v <path>:/Streamer/static/music slorenz/streamer

# list container (optional)
$ docker ps -a
```

Create new mp3 songs
--------------------

1. on mounted volume ("/Streamer/static/music") add your new mp3 files
2. refresh or start page on browser