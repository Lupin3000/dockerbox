FROM ubuntu

MAINTAINER steffen lorenz <slorenz@noreplay.com>

RUN apt-get update -y
RUN apt-get install -y build-essential
RUN apt-get install -y python python-dev python-distribute python-pip

ADD /Streamer /Streamer
RUN pip install -r /Streamer/requirements.txt

EXPOSE 8080

WORKDIR /Streamer
RUN mkdir static/music

CMD ["python", "server.py"]